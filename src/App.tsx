import React from 'react';
import { StrictMode } from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import { garrett, hoodedlady, scummbar } from './images/index';
import picture from './images/omakuva.jpg';
import data from './textfiles/texts.json';
import './App.css';


const App = () => {
  return (
    <StrictMode>

    <BrowserRouter>
      <>
        <NavBar />
        <Routes>
          <Route path='/' element={<Home/>} />
          <Route path='/About' element={<AboutMe/>} />
          <Route path='/Art' element={<Art/>} />
          <Route path='/Code' element={<Code/>} />
          <Route path='/Other' element={<Other/>} />
          <Route path='/Contact' element={<Contact/>} />
          <Route path='*' element={<NotFound/>} />
        </Routes>
      </>

    </BrowserRouter>
    </StrictMode>
  );
}
const NavBar = () => {
  return (
    <>
      <nav>
        <Link className='PageLink' to='/'>Home</Link>
        <Link className='PageLink' to='/About'>About Me</Link>
        <Link className='PageLink' to='/Art'>Art</Link>
        <Link className='PageLink' to='/Code'>Code</Link>
        <Link className='PageLink' to='/Other'>Other</Link>
        <Link className='PageLink' to='/Contact'>Contact</Link>
      </nav>
    </>
  )
};

const Home = () => {
  return (
    <>
      <header >
        <h1>{data.home.header}</h1>
        <h4>{data.home.text}</h4>
      </header>
    </>

  )
};

const AboutMe = () => {
  return (

    <div >

      <header >
        <h1>{data.about.header}</h1>
      </header>

      <div className='FlexBoxContainer'>

        <div className="content">

          <img src={picture}  alt='That is me' width={200} />
          <h4 >
            {data.about.text}
          </h4>

          <ul>
            <li>
              {data.about.languages}

            </li>
            <li>

              {data.about.frameworks}
            </li>
            <li>

              {data.about.tools}
            </li>
          </ul>
          
          <h4 >
            {data.about.text2}
            {data.about.text3}
          </h4>
          <h4 >
            {data.about.text4}
          </h4>
          <h4 >
            {data.about.text5}
          </h4>

        </div>
      </div>
    </div>
  )
};

const Art = () => {
  return (

    <>
      <header >
        <h1>{data.art.header}</h1>
        <h4>{data.art.text}</h4>
      </header>

      <div className='FlexBoxContainer'>

        <div className='content'>
          <img src={garrett} alt='Garrett' />
          <h4 className='imageDesc'>{data.art.garretText}</h4>
        </div>

        <div className='content'>
          <img src={scummbar} alt='Ega Bar' />
          <h4 className='imageDesc'>{data.art.egabarText}</h4>
        </div>

        <div className='content'>
          <img src={hoodedlady} alt='Hooded Lady' />
          <h4 className='imageDesc'>{data.art.hoodedladyText}</h4>
        </div>

      </div>

    </>

  );
};

const Code = () => {
  return (

    <>
      <header >
        <h1>Coding Stuff and Projects</h1>
        <h4>Links and descriptions for my coding projects</h4>
      </header>

      <div className='FlexBoxContainer' style={{flexFlow: "column", textAlign: "left" }}>
        <div className='content'>
          Projects done in Buutti Codematch Academy:
          <ul>
            <li>
            <a className='link' href="https://gitlab.com/hanpekk/website">
               This website! (React/Typescript)
               </a>
            </li>
            <li>
              <a className='link' href="https://gitlab.com/hanpekk/shmup">
                A Simple Space Shooter (C++ / SDL2)
              </a>
            </li>
            <li>
              <a className='link' href="https://gitlab.com/hanpekk/reference-database-app">
                A Simple Source Reference Tool (C++/Qt). My first ever proper project
              </a>
            </li>
            <li>
              <a className='link' href="https://gitlab.com/hanpekk/ryhmatyo-buutti">
                Frontend for a Desktop Program for Organising and Accessing Steam Games (C++/Qt)
              </a>
            </li>
          </ul>
        </div>

        <div className='content'>
          <a className='link' href="https://github.com/hpk-buutti">My Buutti GitHub Page</a>
          <ul>
            <li>
              Integrations for Platform of Trust Multiconnector
            </li>
          </ul>
        </div>

      </div>
    </>
  );
};

const Other = () => {
  return (
    <header >
      <h1>Everything else</h1>
    </header>
  );
};

const Contact = () => {
  return (
    <header >
      <h1>How to contact me</h1>
    </header>
  );

};

const NotFound = () => {
return (
<header>
  <p>Oops! Looks like there is nothing here... How did you get here anyway?</p>
</header>
)
};

export default App;
