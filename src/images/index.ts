import garrett from './pixel/garrett.png';
import scummbar from './pixel/egabar.png';
import hoodedlady from './pixel/hoodedlady_ega.png';

export {
    garrett,
    scummbar,
    hoodedlady
};